output "devops_vm_ip" {
  value = aws_instance.devops_vm.*.public_ip
}