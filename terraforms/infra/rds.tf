resource "aws_db_instance" "rds_test_database" {
  identifier             = var.settings.database.identifier
  allocated_storage      = var.settings.database.allocated_storage
  engine                 = var.settings.database.engine
  engine_version         = var.settings.database.engine_version
  instance_class         = var.settings.database.instance_class
  db_name                = var.settings.database.db_name
  username               = var.db_username
  password               = var.db_password
  db_subnet_group_name   = aws_db_subnet_group.rds_test_db_subnet_group.id
  vpc_security_group_ids = [aws_security_group.infra.id]
  skip_final_snapshot    = var.settings.database.skip_final_snapshot
  backup_retention_period = 7
  backup_window           = "03:00-04:00"
  maintenance_window      = "sun:05:00-sun:06:00"
}

resource "aws_db_snapshot" "rds_snapshot" {
  db_instance_identifier = aws_db_instance.rds_test_database.identifier
  db_snapshot_identifier = "testsnapshot1234"
}