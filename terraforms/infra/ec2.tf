resource "aws_instance" "devops_vm" {
  ami           = "ami-055d15d9cfddf7bd3"
  instance_type = var.instance_size
  key_name = var.ssh_key_pair
#   associate_elastic_ip_address = true
  subnet_id = aws_subnet.public_subnet[0].id
  vpc_security_group_ids = [aws_security_group.infra.id]
  depends_on = [
    aws_eip.devops_vm
  ]
  tags = {
    "Name"= "${var.environment}-${var.name}"
    "Environment"= "${var.environment}"
  }
}
