variable "name" {
  description = "name of your infra"
  type = string
}
variable "environment" {
  description = "name of environment"
  type = string
}
variable "region" {
  description = "region of aws"
  type = string
}

variable "enable_dns_hostname" {
  description = "use dns hostname"
  type = bool
}
variable "enable_dns_support" {
  description = "use dns support"
  type = bool
}
variable "vpc_cidr_block" {
  description = "vpc_cidr block"
  type = string
}
variable "public_subnets_cidr" {
  description = "public subnet cidr"
  type = list(string)
}
variable "private_subnets_cidr" {
  description = "private subnet cidr"
  type = list(string)
}
variable "availability_zone" {
  description = "availability_zone"
  type = list(string)
}
variable "instance_size" {
  description = "size of instance"
  type = string
}
variable "ssh_key_pair" {
  description = "key_pair name"
  type = string
}

variable "settings" {
  description = "Configuration settings"
  type        = map(any)
  default = {
    "database" = {
      identifier          = "test-db"
      allocated_storage   = 10            // storage in gigabytes
      engine              = "mysql"       // engine type
      engine_version      = "8.0.32"      // engine version
      instance_class      = "db.t2.micro" // rds instance type
      db_name             = "rds_test"    // database name
      skip_final_snapshot = true
    }
  }
}


// This variable contains the database master user
// We will be storing this in a secrets file
variable "db_username" {
  description = "Database master user"
  type        = string
  sensitive   = true
  default = "kaizo"
}

// This variable contains the database master password
// We will be storing this in a secrets file
variable "db_password" {
  description = "Database master user password"
  type        = string
  sensitive   = true
  default = "dbpassword"
}