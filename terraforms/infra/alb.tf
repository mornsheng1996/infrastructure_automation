resource "aws_lb" "alb" {
  name               = "devops-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.infra.id]
  subnets            = [for subnet in aws_subnet.public_subnet : subnet.id]
  enable_deletion_protection = false
}

resource "aws_lb_target_group" "alb-target-group" {
  name        = "devops-target-group"
  target_type = "instance"
  port        = 80
  protocol    = "TCP"
  vpc_id      = aws_vpc.vpc.id
}

resource "aws_lb_target_group_attachment" "devops-target-group" {
  target_group_arn = aws_lb_target_group.alb-target-group.arn
  target_id        = aws_instance.devops_vm.id
  port             = 80
}