## DevOps Test Submittion ###
This is a git repo to submit test assignment.

## Instruction to Provistion Infrastructure
---
Prerequisite:

Terraform and AWS CLI need to be installed on machine and access to AWS via CLI should be already setting up.

## Steps to create Infrastructure

1. Clone this repo to local machine
```
cd ~
git clone git@gitlab.com:mornsheng1996/infrastructure_automation.git
```
2. Initialize and setup terraform backend config
```
cd ~
cd infrastructure_automation/terraforms/backend
terraform init --backend-config=../backend/backend.hcl
terraform apply --var-file=../../env.hcl -auto-approve
```
3. Initialize and setup the Infrastructure
```
cd ~
cd infrastructure_automation/terraforms/infra/
terraform init -backend-config=../backend/backend.hcl
terraform apply --var-file=../../env.hcl -auto-approve
```
Once all the steps are already done, the whole infrastructur_automation, and rds backup are done as in the assignment tasks 1, 2 and 4.
